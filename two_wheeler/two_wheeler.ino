/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************
  Note: This only works on Android!
        iOS does not support Bluetooth 2.0 Serial Port Profile
        You may need to pair the module with your smartphone
        via Bluetooth settings. Default pairing password is 1234

  Feel free to apply it to any other example. It's simple!

  NOTE: Bluetooth support is in beta!

  You’ll need:
   - Blynk App (download from AppStore or Google Play)
   - Arduino Nano board
   - Decide how to connect to Blynk
     (USB, Ethernet, Wi-Fi, Bluetooth, ...)

  There is a bunch of great example sketches included to show you how to get
  started. Think of them as LEGO bricks  and combine them as you wish.
  For example, take the Ethernet Shield sketch and combine it with the
  Servo example, or choose a USB sketch and add a code from SendData
  example.
 *************************************************************/

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial
#include <SoftwareSerial.h>
#include <BlynkSimpleSerialBLE.h>
#include <SoftwareSerial.h>
#include <L298N.h>
#include <TimerFreeTone.h>

// Notes
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "fb5aa9e5644244d08e300aa439e3cdf2";

SimpleTimer timer;

// Define LED pin
#define headlight 3

// Define buzzer pin
#define buzzer 10
int sound = 1;
long buzzerfrequency = 100;

// Define the motor pins for L298N
#define ENA 9
#define IN1 8
#define IN2 7

#define ENB 11
#define IN3 4
#define IN4 12

// Define bluetooth pin
#define TxD 5
#define RxD 6

// Define bluetooth objects
SoftwareSerial SwSerial(RxD, TxD); // RX, TX
SoftwareSerial SerialBLE(RxD, TxD); // RX, TX

//create a motor instance
L298N motorA(ENA, IN1, IN2);
L298N motorB(ENB, IN4, IN3);

// timer
int mytimer;

void setup()
{
  // startup test
  startuptone(buzzer);
  motorA.forward();
  motorB.forward();
  delay(100);
  motorA.stop();
  motorB.stop();

  // Debug console
  Serial.begin(9600);

  SerialBLE.begin(9600);
  Blynk.begin(SerialBLE, auth);

  Serial.println("Waiting for connections...");
}

void loop()
{
  timer.run();
  Blynk.run();
}

BLYNK_WRITE(V3) {
  analogWrite(headlight, param.asInt());
}

BLYNK_WRITE(V8) {
  buzzerfrequency = param.asInt();
}

BLYNK_WRITE(V9) {
  sound = param.asInt();
}

BLYNK_WRITE(V10) {
  if (param.asInt()) {
    if (sound == 1) {
      carhorn(buzzer, buzzerfrequency);
    } else if (sound == 2) {
      intruderSiren(buzzer);
    } else if (sound == 3) {
      policeSiren(buzzer);
    } else if (sound == 4) {
      mariotheme(buzzer);
    } else if (sound == 5) {
      coin(buzzer);
    } else if (sound == 6) {
      fireball(buzzer);
    } else if (sound == 7) {
      oneup(buzzer);
    }
  }
}

BLYNK_WRITE(V20) {
  motorA.setSpeed(param.asInt());
  motorB.setSpeed(param.asInt());
}

BLYNK_WRITE(V21) {
  if (param.asInt()) {
    motorB.forward();
  } else {
    motorB.stop();
  }
}

BLYNK_WRITE(V22) {
  if (param.asInt()) {
    motorA.forward();
  } else {
    motorA.stop();
  }
}

BLYNK_WRITE(V23) {
  if (param.asInt()) {
    motorB.backward();
  } else {
    motorB.stop();
  }
}

BLYNK_WRITE(V24) {
  if (param.asInt()) {
    motorA.backward();
  } else {
    motorA.stop();
  }
}

void startuptone(int pin) {
  int melody[] = {
    NOTE_E7, NOTE_E7, 0, NOTE_E7,
    0, NOTE_C7, NOTE_E7, 0,
    NOTE_G7, 0, 0,  0,
    NOTE_G6, 0, 0, 0,

    NOTE_C7, 0, 0, NOTE_G6,
    0, 0, NOTE_E6, 0,
    0, NOTE_A6, 0, NOTE_B6,
    0, NOTE_AS6, NOTE_A6, 0,

    NOTE_G6, NOTE_E7, NOTE_G7,
    NOTE_A7, 0, NOTE_F7, NOTE_G7,
    0, NOTE_E7, 0, NOTE_C7,
    NOTE_D7, NOTE_B6, 0, 0,

    NOTE_C7, 0, 0, NOTE_G6,
    0, 0, NOTE_E6, 0,
    0, NOTE_A6, 0, NOTE_B6,
    0, NOTE_AS6, NOTE_A6, 0,

    NOTE_G6, NOTE_E7, NOTE_G7,
    NOTE_A7, 0, NOTE_F7, NOTE_G7,
    0, NOTE_E7, 0, NOTE_C7,
    NOTE_D7, NOTE_B6, 0, 0
  };

  int tempo[] = {
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,

    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,

    9, 9, 9,
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,

    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,

    9, 9, 9,
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,
  };
  int pause[] = { 150, 150, 300, 150, 300, 3000 };
  for (int thisNote = 0; thisNote < (sizeof(melody) / sizeof(int)); thisNote++) {
    analogWrite(headlight, melody[thisNote]);
    int noteDuration = 1000 / tempo[thisNote];
    TimerFreeTone(buzzer, melody[thisNote], 1000 / tempo[thisNote]);
    digitalWrite(headlight, LOW);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
  }
  digitalWrite(headlight, LOW);
}

void carhorn(int pin, long frequency) {
  TimerFreeTone(pin, frequency, 300);
}

void intruderSiren(int pin) {
  TimerFreeTone(pin, 400, 500);
  TimerFreeTone(pin, 800, 500);
  TimerFreeTone(pin, 400, 500);
  TimerFreeTone(pin, 800, 500);
}

void policeSiren(int pin) {
  for (int i = 150; i < 1800; i++) { //upward tone
    TimerFreeTone(pin, i, 1);
  }
  for (int i = 1800; i > 150; i--) { //downward tone
    TimerFreeTone(pin, i, 1);
  }
}

void mariotheme(int pin) {
  int melody[] = {
    NOTE_E7, NOTE_E7, 0, NOTE_E7,
    0, NOTE_C7, NOTE_E7, 0,
    NOTE_G7, 0, 0,  0,
  };

  int tempo[] = {
    12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12,
  };
  int pause[] = { 150, 150, 300, 150, 300, 3000 };
  for (int thisNote = 0; thisNote < (sizeof(melody) / sizeof(int)); thisNote++) {
    int noteDuration = 1000 / tempo[thisNote];
    TimerFreeTone(buzzer, melody[thisNote], 1000 / tempo[thisNote]);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
  }
}

void coin(int pin) {
  TimerFreeTone(pin, NOTE_B5, 100);
  TimerFreeTone(pin, NOTE_E6, 450);
}

void fireball(int pin) {
  TimerFreeTone(pin, NOTE_G4, 35);
  TimerFreeTone(pin, NOTE_G5, 35);
  TimerFreeTone(pin, NOTE_G6, 35);
}

void oneup(int pin) {
  TimerFreeTone(pin, NOTE_E6, 125);
  TimerFreeTone(pin, NOTE_G6, 125);
  TimerFreeTone(pin, NOTE_E7, 125);
  TimerFreeTone(pin, NOTE_C7, 125);
  TimerFreeTone(pin, NOTE_D7, 125);
  TimerFreeTone(pin, NOTE_G7, 125);
}

