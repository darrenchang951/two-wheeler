# two-wheeler
A two-wheel, bluetooth controlled RC with lots of fun.

# Components
- Motor driver [L298N]
- Control Unit [Arduino Nano]
- Bluetooth [HC-05] (it should be safe to input 5v into Rx and Tx)
- Two 12V motors at 60 rpm
- One 5v LED
- A generic speaker
- x3 18650 batteries

# Libraries
- [timer-free-tone](https://bitbucket.org/teckel12/arduino-timer-free-tone/src)
- blynk
- l298n

timer-free-tone is used to solve function tone()'s conflict with D3 and D11.

# Breadboard view
![image](two_wheeler_bb.jpg)